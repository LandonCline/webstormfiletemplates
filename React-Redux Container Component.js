import React from 'react';
import {connect} from 'react-redux';
import ${NAME} from '${PAGE_REF}';


const mapStateToProps = state => {
    return {
        ${STATE}
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            ${ACTION}
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(${NAME});